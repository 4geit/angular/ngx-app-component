import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxMaterialModule } from '@4geit/ngx-material-module';

import { NgxAppComponent } from './ngx-app.component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    RouterModule
  ],
  declarations: [
    NgxAppComponent
  ],
  exports: [
    NgxAppComponent
  ]
})
export class NgxAppModule { }
