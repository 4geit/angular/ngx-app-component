import { Component, OnInit } from '@angular/core';

import { NgxPageService } from '@4geit/ngx-page-service';
import { NgxAuthService } from '@4geit/ngx-auth-service';

@Component({
  selector: 'app-root',
  template: require('pug-loader!./ngx-app.component.pug')(),
  styleUrls: ['./ngx-app.component.scss']
})
export class NgxAppComponent implements OnInit {

  constructor(
    private pageService: NgxPageService,
    private authService: NgxAuthService
  ) { }

  ngOnInit() {
  }

}
