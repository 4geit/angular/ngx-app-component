import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxAppComponent } from './ngx-app.component';

describe('app', () => {
  let component: app;
  let fixture: ComponentFixture<app>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ app ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(app);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
